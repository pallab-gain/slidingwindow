/*! \file slidingwindow.cpp
 *  \author Pallab Gain <pallab.gain@gmail.com>
 *  \brief Sliding window for finding median
 *  \details A sliding window implementation for finding media
 *  ( Sliding window ) to find median value for collections of delay.
 *
 *  Original Task:
 *  Implement a sliding window contains limited amount
 *  of items and provides addDelay and getMedian interfaces. The
 *  former interface adds a delay value to -, the latter interface
 *  returns the median of the delays calculated over the items from
 *  the sliding window. You must limit your sliding window regarding
 *  to the number of items it can contain.
 *
 *
 *
 *  Solution details :
 *  1. Use two Red Black Tree to store delays.
 *      - max_heap for delays smaller than current median,
 *      - min_heap for delays bigger than or equal to the current median.
 *
 *  2. Two heaps will store data in such a way that
 *      - Delays stored in max_heap will always be in ascending order
 *      - Delays stored in min_heap will always be in descending order
 *
 *  3. Balance the two heaps in such a way that
 *      - the size of min_heap will be equal to the size of max_heap for even number of elements
 *      - the size of min_heap is at most bigger by one than max_heap when there is odd number of elements.
 *
 *  4. Finding Median
 *      - If size of two heaps is odd ( there are currently odd numbers of delay(s) in the sliding window )
 *          - Median will be the first element from the min_heap
 *      - If size of two heaps is even ( there are currently even numbers of delay(s) in the sliding window )
 *          - Media will be the sum divid by two of the first elements from min_heap, and max_heap
 *
 *  Run time complexity of the solution :
 *
 *  Given that
 *      - There will be n operations
 *      - Sliding window size/length is m
 *
 *  Overall run time complexity O(n*log(m))
 *
 *
 */
#include <iostream>
#include <set>
#include <deque>
#include <vector>
#include <assert.h>

using namespace std;


/**
 *  SlidingWindow class.
 *
 */

class SlidingWindow {
private:
/* Sliding window pair. Take delay, and current cursor position */
#define SLIDING_WINDOW pair<double, int>
/* EPS value. Arithmetic precious that will be guarantee in all operations in this class */
#define EPS 1e-9

    /*!\brief Method to add a new valid token for authenticating
     * @param a an double value
     * @returns The absolute value
     * */
    inline static double abs(double val) {
        return (EPS < val) ? val : -val;
    };

    /*!\brief An comparator that will be used for min heap building. Data will be stored in ascending order when using this comparator
     * @param A SLIDING_WINDOW value
     * @param A SLIDING_WINDOW value
     * @returns A bool true value if first item is strictly less than second item
     * */
    struct minheap_comparator {
        bool operator()(const SLIDING_WINDOW &lhs, const SLIDING_WINDOW &rhs) const {
            /*If both are equal (difference is less than or equal to EPS)*/
            if (abs(lhs.first - rhs.first) <= EPS) {
                return lhs.second < rhs.second;
            }
            return lhs.first < rhs.first;
        }
    };

    /*!\brief An comparator that will be used for max heap building. Data will be stored in descending order when using this comparator
    * @param A SLIDING_WINDOW value
    * @param A SLIDING_WINDOW value
    * @returns A bool true value if first item is greater than or equal to second item
    * */
    struct maxheap_comparator {
        bool operator()(const SLIDING_WINDOW &lhs, const SLIDING_WINDOW &rhs) const {
            /*If both are equal (difference is less than or equal to EPS)*/
            if (abs(lhs.first - rhs.first) <= EPS) {
                return lhs.second < rhs.second;
            }
            return !(lhs.first < rhs.first);
        }
    };

    /*!\brief Resize min heap, and max heap based on predefined condition.
     * If max heap is greater than min heap ; balance the two heap
     * Resize sliding window based of window side
     **/
    inline void resizeInternal() {
        /* According to the implementation min heap will be always be greater or equal to max heap. Therefore to balance the heaps move the
         * largest element ( first element ) from max heap to min heap */
        if (max_heap.size() > min_heap.size()) {
            SLIDING_WINDOW tmp(max_heap.begin()->first, max_heap.begin()->second);
            max_heap.erase(max_heap.begin());
            min_heap.insert(tmp);
        }
        /* According to the implementation min heap can be at most one element more in size. Therefore to balance the heaps move the smallest (first element)
         * element from min heap to max heap */
        if (min_heap.size() - max_heap.size() > 1) {
            SLIDING_WINDOW tmp(min_heap.begin()->first, min_heap.begin()->second);
            min_heap.erase(min_heap.begin());
            max_heap.insert(tmp);
        }

        /* Remove extra element, and balance the size with current sliding window length*/
        if (items.size() > window_size) {
            SLIDING_WINDOW tmp = (SLIDING_WINDOW) items.front();
            items.pop_front();

            if (!max_heap.erase(tmp)) {
                min_heap.erase(tmp);
            }

            resizeInternal();
        }
    }

    /*!\brief Get current median value
     **/
    inline double getMedianInternal() {
        if ((max_heap.size() + min_heap.size()) & 1) {
            return min_heap.begin()->first;
        }
        return (max_heap.begin()->first + min_heap.begin()->first) / 2.;
    }

    /* Data stored in this container will be in descending order. Using C++ multiset; An implementation of self-balancing binary trees, typically red-blacktree .
     * Insertion, and deletion will be log(size of container)
     * */
    multiset<SLIDING_WINDOW, maxheap_comparator> max_heap;

    /* Data stored in this container will be in ascending order. Using C++ multiset; An implementation of self-balancing binary trees, typically red-blacktree .
     * Insertion, and deletion will be log(size of container)
     * */
    multiset<SLIDING_WINDOW, minheap_comparator> min_heap;

    /* An dequeue implementation. Insertion, and deletion from top, and front will be constant time . */
    deque<SLIDING_WINDOW > items;

    /* Sliding window size */
    size_t window_size;

    /* Current cursor. Track list of total inserted item */
    size_t n_items;


public :
    /*!\brief SlidingWindow constructor
     * @param _window_size size_t take length of current sliding window
     * */
    SlidingWindow(size_t _window_size) {
        window_size = _window_size;
        n_items = 0;
        items.clear();
        max_heap.clear();
        min_heap.clear();
    }

    /*!\brief Resize the sliding window length
     * @param _window_size size_t take length of current sliding window
     * */
    inline void resize(size_t _window_size) {
        window_size = _window_size;
        resizeInternal();
    }

    /*!\brief Print the current media, and content of both max, and min heap contents
     * */
    inline void print() {
        cout << "Current median " << getMedian() << "\n";
        cout << "max queue -> \n";
        for (auto x = max_heap.begin(); x != max_heap.end(); x++) {
            cout << x->first << " " << x->second << "\n";
        }
        cout << "min queue -> \n";
        for (auto x = min_heap.begin(); x != min_heap.end(); x++) {
            cout << x->first << " " << x->second << "\n";
        }
        cout << string(10, '.') << "\n";
    }

    /*!\brief Provide the current media of sliding window
     * @return double representation of current media value
     * */
    inline double getMedian() {
        double retval = getMedianInternal();
        if (max_heap.size() + min_heap.size() <= 1) {
            return -1;
        }
        return retval;
    }

    /*!\brief Append a new delay item in sliding window container
    * @param delay Double representation of delay value
    * */
    inline void addDelay(double delay) {
        SLIDING_WINDOW item(delay, n_items);
        n_items += 1;
        /*If current inserted delay is less than current median; move it to max heap */
        if (delay < getMedianInternal()) {
            max_heap.insert(item);
        } else {
            min_heap.insert(item);
        }
        items.push_back(item);

        resizeInternal();
    }

};


int main() {

    if (!freopen("inputs/getMedian-attachments/Round1test1.csv", "r", stdin))
        puts("error opening file in "), assert(0);

    vector<double> numbers;
    double in;
    const int min_window_size = 3;
    const int max_window_size = 3;

    while (cin >> in) {
        numbers.push_back(in);
    }
    for (int window_size = min_window_size; window_size <= max_window_size; window_size += 1) {
        SlidingWindow slidingWindow(window_size);
        for (int i = 0, upto = numbers.size(); i < upto; i += 1) {
            slidingWindow.addDelay(numbers[i]);
            cout << slidingWindow.getMedian() << "\n";
        }

    }
    return 0;
}