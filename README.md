## README ##
A sliding Window implementation for finding median value.

## Problem Description ##
Implement a sliding window contains limited amount of items and provides addDelay and getMedian interfaces. 
The former interface adds a delay value to -, the latter interface returns the median of the delays calculated over the items from
the sliding window. You must limit your sliding window regarding to the number of items it can contain.

##  Solution approach ##
1. Use two Red Black Tree to store delays.
    - max_heap for delays smaller than current median,
    - min_heap for delays bigger than or equal to the current median.
2. Two heaps will store data in such a way that
    - Delays stored in max_heap will always be in ascending order
    - Delays stored in min_heap will always be in descending order
3. Balance the two heaps in such a way that
    - the size of min_heap will be equal to the size of max_heap for even number of elements
    - the size of min_heap is at most bigger by one than max_heap when there is odd number of elements.
4. Finding Median
    - If size of two heaps is odd ( there are currently odd numbers of delay(s) in the sliding window )
        - Median will be the first element from the min_heap
    - If size of two heaps is even ( there are currently even numbers of delay(s) in the sliding window )
        - Media will be the sum divid by two of the first elements from min_heap, and max_heap
    
## Run time complexity of the approach ##
- Given that
    - There will be ```n``` operations
    - Sliding window size/length is ```m```
- Overall run time complexity ```O(n*log(m))```

## API Documentation ##
[https://dazzling-scarf.surge.sh/](https://dazzling-scarf.surge.sh/)

## Contact ##
Developed by [pallab-gain](https://bitbucket.org/pallab-gain)